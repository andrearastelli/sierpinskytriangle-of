#pragma once

#include "ofMain.h"
#include <list>
#include <array>
#include <iostream>
#include <cmath>

const int shapeSides = 5;

class ofApp : public ofBaseApp
{
public:
    void setup();
    void update();
    void draw();

    void keyPressed(int key);
    void keyReleased(int key);
    void mouseMoved(int x, int y );
    void mouseDragged(int x, int y, int button);
    void mousePressed(int x, int y, int button);
    void mouseReleased(int x, int y, int button);
    void mouseEntered(int x, int y);
    void mouseExited(int x, int y);
    void windowResized(int w, int h);
    void dragEvent(ofDragInfo dragInfo);
    void gotMessage(ofMessage msg);
		
private:
    std::array<ofVec2f, shapeSides> points;
    int pointCounter = 0;
    std::list<ofVec2f> shapePoints;
    int prevDice = 1;

};
