#include "ofApp.h"

//--------------------------------------------------------------
void ofApp::setup()
{
    // ofSetFrameRate(120);
    ofSetVerticalSync(false);

    shapePoints.push_back(ofVec2f(ofRandom(1024), ofRandom(768)));
}

//--------------------------------------------------------------
void ofApp::update()
{
    for (int i=0; i<1000; ++i)
    {
        if (pointCounter == shapeSides)
        {
            int rndDice = static_cast<int>(ofRandom(0, shapeSides));

            auto compareCurrent = rndDice + 1;
            auto comparePrev = prevDice + 1;
            int mod = std::div(compareCurrent, comparePrev).rem;

            if (rndDice != prevDice)
            {
                auto shapeDirection = points[rndDice];
                auto newPoint = shapeDirection.middle(shapePoints.back());

                shapePoints.push_back(newPoint);

                prevDice = rndDice;
            }
        }
    }
}

//--------------------------------------------------------------
void ofApp::draw()
{
    ofClear(ofColor::grey);
    std::string text = "Iterations: " + ofToString(shapePoints.size());
    ofDrawBitmapString(text, 20, 20);

    ofSetColor(ofColor::red);
    for (auto p : points)
    {
        ofDrawCircle(p.x, p.y, 5);
    }

    ofSetColor(ofColor::black);
    for (auto p : shapePoints)
    {
        ofDrawCircle(p.x, p.y, 0.5);
    }
}

//--------------------------------------------------------------
void ofApp::keyPressed(int key)
{
    if (key == OF_KEY_PAGE_DOWN)
        shapePoints.clear();
        points.empty();
        pointCounter = 0;
}

//--------------------------------------------------------------
void ofApp::keyReleased(int key){}

//--------------------------------------------------------------
void ofApp::mouseMoved(int x, int y ){}

//--------------------------------------------------------------
void ofApp::mouseDragged(int x, int y, int button){}

//--------------------------------------------------------------
void ofApp::mousePressed(int x, int y, int button)
{
    if (pointCounter < shapeSides)
        points[pointCounter++] = ofVec2f(x, y);
}

//--------------------------------------------------------------
void ofApp::mouseReleased(int x, int y, int button){}

//--------------------------------------------------------------
void ofApp::mouseEntered(int x, int y){}

//--------------------------------------------------------------
void ofApp::mouseExited(int x, int y){}

//--------------------------------------------------------------
void ofApp::windowResized(int w, int h){}

//--------------------------------------------------------------
void ofApp::gotMessage(ofMessage msg){}

//--------------------------------------------------------------
void ofApp::dragEvent(ofDragInfo dragInfo){}
